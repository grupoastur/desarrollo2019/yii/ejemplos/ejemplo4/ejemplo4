<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Felicidades!</h1>

        <p class="lead">Has accedido a yii.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Entra en Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
            </div>
            <?=
            Html::img("@web/img/foto.jpg",[
                "class"=>"img-responsive img-circle center-block",
                "alt"=>"foto"
            ]);
            
            ?>
            <div class="col-lg-4">
                

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
            </div>
        </div>

    </div>
</div>
