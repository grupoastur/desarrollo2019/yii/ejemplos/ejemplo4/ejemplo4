<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Productos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
      public function actionOfertas()
    {
        return $this->render('ofertas');
      
    }
       public function actionProductos()
    {
        $registro=Productos::find()->all();
        return $this->render('productos',
                    ['datos'=>$registro]);
        
    }

    public function actionCategorias(){
        return $this->render('categorias');
    }
    
    public function actionContacto()
    {
        return $this->render('contacto');
    }
    
      public function actionNosotros()
    {
        return $this->render('nosotros');
    }
      public function actionEstamos()
    {
        return $this->render('estamos');
    }
      public function actionSomos()
    {
        return $this->render('somos');
    }
      public function actionProductosb()
    {
        return $this->render('productosb');
    }
      public function actionInformacion()
    {
        return $this->render('informacion');
    }

   
}
